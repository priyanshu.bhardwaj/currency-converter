import React from 'react'
import { RotatingLines } from 'react-loader-spinner'

type loaderProps = {
    message?: string
}

const Loader = (props:loaderProps) => {
  return (
      <div style={{display:'flex',alignItems:"center",gap:'0.5vw',justifyContent:"center"}}>
          <RotatingLines
  strokeColor="lightgrey"
  strokeWidth="5"
  animationDuration="0.75"
  width="20"
  visible={true}
          />
          <div style={{color:'lightgrey'}}>{ props.message}</div>
    </div>
  )
}

export default Loader