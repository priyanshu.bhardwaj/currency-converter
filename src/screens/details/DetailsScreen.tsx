import React from 'react'
import MiddleSegment from '../../components/middle/MiddleSegment'
import NavBar from '../../components/nav/NavBar'
import styles from './DetailsScreen.module.css'

const DetailsScreen = () => {
  return (
    <div className={styles.homeContainer}>
      <NavBar />
      <div>
        <MiddleSegment />
      </div>
    </div>
  )
}

export default DetailsScreen