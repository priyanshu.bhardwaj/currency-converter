import React from 'react'
import styles from './HomeScreen.module.css'
import NavBar from '../../components/nav/NavBar'
import MiddleSegment from '../../components/middle/MiddleSegment'

const HomeScreen = () => {
  return (
    <div className={styles.homeContainer}>
      <NavBar />
      <div>
        <MiddleSegment />
      </div>
    </div>
  )
}

export default HomeScreen