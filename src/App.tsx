import React, { useEffect } from 'react';
import { Route, Routes } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import { fetchSymbols } from './redux/features/symbolsSlice';
import { useAppDispatch } from './redux/store';
import DetailsScreen from './screens/details/DetailsScreen';
import HomeScreen from './screens/home/HomeScreen';

function App() {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchSymbols())
  },[])
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<HomeScreen />} />
        <Route path='/details' element={<DetailsScreen/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
