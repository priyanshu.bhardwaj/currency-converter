import React from 'react'
import styles from './GreyDropdown.module.css'
import { symbols } from '../../dummyData'
import { useAppDispatch, useAppSelector } from '../../redux/store'

type dropDownType = {
    title: string,
    action: (payload: string) => any,
    value: string,
    disabled?: boolean
}

const GreyDropdown = (props: dropDownType) => {
    const dispatch = useAppDispatch()
    const { data } = useAppSelector(state => state.symbols)

  return (
      <div>
          <div>{ props.title} :</div>
          <select disabled={props.disabled} value={props.value} className={styles.select} onChange={e => dispatch(props.action(e.target.value))} >
              <option value='Select' selected hidden>Select</option>
              {
                  Object.keys(data).map((item,index) => {
                      return (
                          <option key={index} value={item}>{ item}</option>
                      )
                  })
              }
          </select>
    </div>
  )
}

export default GreyDropdown