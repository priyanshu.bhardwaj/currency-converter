import React,{useEffect, useState} from 'react'
import GreyDropdown from '../dropdowns/GreyDropdown'
import BlackInput from '../inputs/black/BlackInput'
import styles from './MiddleSegment.module.css'
import {MdOutlineSwapHoriz} from 'react-icons/md'
import BlueButton from '../buttons/blue/BlueButton'
import { changeAmount, changeFrom, changeTo, fetchConversionRate, fetchConvertedAmount, swapValues } from '../../redux/features/dataSlice'
import { useAppDispatch, useAppSelector } from '../../redux/store'
import Loader from '../../utils/Loader'
import TitleBar from '../nav/TitleBar'
import {useNavigate} from 'react-router-dom'

const MiddleSegment = () => {
    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const [disableButton,setDisableButton] = useState(true)
    const { convertedFrom, convertedTo, initialAmount, loading, conversionRate, convertedAmount, conversionLoader,  } = useAppSelector(state => state.data)
    useEffect(() => {
        if (convertedFrom && convertedTo) {
            dispatch(fetchConversionRate())
        }
    }, [convertedFrom, convertedTo])
    const handleClick = () => {
        dispatch(fetchConvertedAmount()).unwrap().then(() => navigate('/details'))
    }
    useEffect(() => {
        if (initialAmount && parseInt(initialAmount) > 0) {
            setDisableButton(false)
        } else {
            setDisableButton(true)
        }
    },[initialAmount])
    return (
        <>
            <TitleBar/>
    <div className={styles.container}>
        <div className={styles.layer}>
              <div className={styles.leftLayer}>
                  <BlackInput action={changeAmount} value={initialAmount} />
                  <div className={styles.rateDiv}>{ loading ? <Loader /> : conversionRate ?  <div>1 {convertedFrom} = {conversionRate} {convertedTo}</div> : null}</div>
             </div>
              <div className={styles.rightLayer}>
                  <div className={styles.swapLayer}>
                    <GreyDropdown disabled={disableButton} title='From' action={changeFrom} value={convertedFrom} />
                    <div className={styles.swapContainer}>
                        <MdOutlineSwapHoriz size={50} className={ styles.swapButton} onClick={()=>dispatch(swapValues())} />
                    </div>
                    <GreyDropdown disabled={disableButton} title='To' action={changeTo} value={convertedTo} />
                  </div>
                  <BlueButton title='Convert' action={handleClick} disabled={disableButton} />
                  <div className={styles.resultLayer}>
                      <div className={styles.resultDiv}>{convertedAmount ? <>{convertedAmount} { convertedTo}</> : conversionLoader ? <Loader/> : null }</div>
                      {/* <div className={styles.detailsContainer}>
                          <BlueButton title='More Details' action={fetchConvertedAmount}/>
                      </div> */}
                </div>
            </div>
        </div>
    </div>
        </>
  )
}

export default MiddleSegment