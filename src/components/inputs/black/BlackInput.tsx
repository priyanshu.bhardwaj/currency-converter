import React from 'react'
import { useAppDispatch } from '../../../redux/store'
import styles from './BlackInput.module.css'

type blackInputPropps = {
    action: (payload: number) => any,
    value: string
}

const BlackInput = (props: blackInputPropps) => {
    const dispatch = useAppDispatch()
  return (
      <div>
          <div>Amount :</div>
          <input type='number' min='0' className={styles.input} value={props.value} onChange={e=>dispatch(props.action(parseFloat(e.target.value)))} />
    </div>
  )
}

export default BlackInput