import React from 'react'
import { changeFrom, changeTo } from '../../../redux/features/dataSlice'
import { useAppDispatch } from '../../../redux/store'
import styles from './BlackButton.module.css'

type blackButtonProps = {
    from: string,
    to:string
}

const BlackButton = (props: blackButtonProps) => {
    const dispatch = useAppDispatch()
    const handleClick = () => {
        dispatch(changeFrom(props.from))
        dispatch(changeTo(props.to))
    }
  return (
      <button className={styles.button} onClick={handleClick} >{props.from}-{props.to} details</button>
  )
}

export default BlackButton