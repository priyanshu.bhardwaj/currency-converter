import React from 'react'
import { useAppDispatch } from '../../../redux/store'
import styles from './BlueButton.module.css'

type blueButtonProps = {
    title: string,
    action: () => void,
    disabled?: boolean
}

const BlueButton = (props: blueButtonProps) => {
    const dispatch = useAppDispatch()
  return (
      <button disabled={props.disabled} className={styles.button} onClick={props.action} >{ props.title}</button>
  )
}

export default BlueButton