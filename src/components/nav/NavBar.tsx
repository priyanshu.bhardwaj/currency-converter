import React from 'react'
import BlackButton from '../buttons/black/BlackButton'
import styles from './NavBar.module.css'
import {TbShieldLockFilled} from 'react-icons/tb'

const NavBar = () => {
  return (
      <div className={styles.navContainer}>
          <TbShieldLockFilled size={80} color='#0072ff' />
          <div className={styles.buttonContainer}>
              <BlackButton from='EUR' to='USD' />
              <BlackButton from='EUR' to='GBP' />
          </div>
    </div>
  )
}

export default NavBar