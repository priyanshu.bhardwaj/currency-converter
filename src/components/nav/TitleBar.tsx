import React from 'react'
import BlueButton from '../buttons/blue/BlueButton'
import styles from './TitleBar.module.css'
import {useLocation, useNavigate} from 'react-router-dom'
import { useAppSelector } from '../../redux/store'

const TitleBar = () => {
    const location = useLocation()
    const { convertedFrom } = useAppSelector(state => state.data)
    const {} = useAppSelector(state => state.symbols)
    const navigate = useNavigate()
    const handleClick = () => { 
        navigate('/')
     }
  return (
    <div className={styles.header}>
          <div className={styles.headerTitle}>
              {
                  location.pathname === '/' ? 
                      <div>Currency Exchanger</div>
                      :
                      <div>{convertedFrom} - { }</div>
              }
          </div>
          {
              location.pathname==='/details' && <div>
            <BlueButton title='Back to Home' action={handleClick} />
          </div>
          }
        </div>
  )
}

export default TitleBar