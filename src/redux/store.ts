import { configureStore } from "@reduxjs/toolkit";
import DataReducer from "./features/dataSlice";
import SymbolReducer from './features/symbolsSlice'
import { useDispatch, TypedUseSelectorHook, useSelector } from 'react-redux'
import logger from "redux-logger";

export const store = configureStore({
  reducer: {
    data: DataReducer,
    symbols: SymbolReducer
  },
  middleware: (getDefaultMiddleWare) => getDefaultMiddleWare().concat(logger),
});

export const useAppDispatch: () => typeof store.dispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<ReturnType<typeof store.getState>> = useSelector