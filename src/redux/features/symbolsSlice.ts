import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {api as axios} from "../../axios.config";

type stateType = {
    loading: boolean,
    data: object
    
};

const initialState: stateType = {
    loading: false,
    data: {}
};

export const fetchSymbols = createAsyncThunk('symbols/fetchSymbols', async (data,{fulfillWithValue,rejectWithValue}) => {
    try {
        const response = await axios.get("/symbols")
        return fulfillWithValue(response.data.symbols)
    } catch (error) {
        console.log(error)
    }
})

const symbolsSlice = createSlice({
  name: "symbols",
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(fetchSymbols.pending, (state) => {
          state.loading = true
        })
        builder.addCase(fetchSymbols.fulfilled, (state,action) => {
            state.loading = false
            state.data = action.payload
        })
        builder.addCase(fetchSymbols.rejected, (state) => {
          state.loading = false
      })
  }
});

export default symbolsSlice.reducer;
