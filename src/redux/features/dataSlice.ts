import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {api as axios} from "../../axios.config";

type stateType = {
  loading: boolean,
  conversionLoader: boolean,
  error: '',
  convertedFrom: string,
  convertedTo: string,
  initialAmount: string,
  convertedAmount: string,
  conversionRate: string
};

const initialState: stateType = {
  loading: false,
  conversionLoader: false,
  error:'',
  convertedFrom: "",
  convertedTo: "",
  initialAmount: "",
  convertedAmount: '',
  conversionRate:''
};

export const fetchConversionRate = createAsyncThunk('data/fetchConversionRate', async (data,{getState,fulfillWithValue,rejectWithValue}) => {
  try {
    let state: any = getState()
    const response = await axios.get(`/latest?symbols=${state.data.convertedTo}&base=${state.data.convertedFrom}`)
    console.log(response.data.rates[state.data.convertedTo])
    return fulfillWithValue(response.data.rates[state.data.convertedTo])
  } catch (error) {
    console.log(error)
  }
})

export const fetchConvertedAmount = createAsyncThunk('data/fetchConvertedAmount', async (data,{fulfillWithValue,rejectWithValue,getState}) => {
  try {
    let state: any = getState()
    const response = await axios.get(`/convert?to=${state.data.convertedTo}&from=${state.data.convertedFrom}&amount=${state.data.initialAmount}`)
    return fulfillWithValue(response.data.result)
  } catch (error) {
    console.log(error)
    return rejectWithValue(error)
  }
})

const dataSlice = createSlice({
  name: "data",
  initialState,
  reducers: {
    changeFrom: (state, action) => {
      state.convertedFrom = action.payload;
    },
    changeTo: (state, action) => {
      state.convertedTo = action.payload;
    },
    changeAmount: (state, action) => {
      state.initialAmount = action.payload;
    },
    swapValues: (state) => {
      let temp = state.convertedFrom
      state.convertedFrom = state.convertedTo
      state.convertedTo = temp
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchConversionRate.pending, (state) => {
      state.loading = true
    })
    builder.addCase(fetchConversionRate.fulfilled, (state, action) => {
      state.loading = false
      state.conversionRate = action.payload
    })
    builder.addCase(fetchConversionRate.rejected, (state, action) => {
      state.loading = false
    })

    builder.addCase(fetchConvertedAmount.pending, (state) => {
      state.conversionLoader = true
    })
    builder.addCase(fetchConvertedAmount.fulfilled, (state, action) => {
      state.conversionLoader = false
      state.convertedAmount = action.payload
    })
    builder.addCase(fetchConvertedAmount.rejected, (state, action) => {
      state.conversionLoader = false
    })
  }
});

export default dataSlice.reducer;
export const {changeAmount, changeFrom, changeTo, swapValues} = dataSlice.actions;
